import { Component } from "react";
import "./css/main.css";
import "./css/util.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import arrowSvg from "./img/down-arrow-svgrepo-com.svg";
import RegForm from "../RegForm/RegForm";

class BoltPage extends Component {
    render() {
        return (
            <div className="limiter">
		        <div className="container-login100">
			        <div className="login100-more">
                        <div className="main-title p-b-25">
                            Make money driving with Bolt    
                        </div>
                        <div className="main-subtitle p-b-25">
                            Become a Bolt driver, set your schedule and earn extra money by driving!
                        </div>
                        <div className="main-more-info p-t-20">
                            <p className="p-r-30">LEARN MORE</p>
                            <img src={arrowSvg} />
                        </div>
                    </div>
        			<div className="wrap-login100 p-l-30 p-r-30 p-t-30 p-b-30">
			            <RegForm />
			        </div>
		        </div>
	        </div>
        );
  }
}
 
export default BoltPage;