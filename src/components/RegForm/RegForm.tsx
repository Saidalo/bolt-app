import { Component } from "react";
import Select from "react-dropdown-select";
import getUnicodeFlagIcon from 'country-flag-icons/unicode'
import { countries as codes } from "../../mocks/countryCodes";
 
class RegForm extends Component<any, any> {
    
    constructor(props: any) {
        super(props);
        this.state = {
            errors: {},
            fields: {}
        }
    }

    handleChange(field:string, e:any) {
        let fields = this.state.fields;
        if (Array.isArray(e)) {
            fields[field] = e[0]?.label.props ? e[0]?.label.props?.children[0] : e[0]?.label;
        } else {
            fields[field] = e.target.type == 'checkbox' ? e.target.checked : e.target.value;
        }
        this.setState({ fields });
    }

    render() {
        let mappedCodes: City[];
        mappedCodes = codes.map((elem) => {
            return {
                label: <div>
                    {elem.dial_code}
                    {getUnicodeFlagIcon(elem.code)}
                </div>,
                value: elem.code
            };
        });
        var cities: CityObj[] = [
            {
                label: "Tallin",
                value: "EST"
            },
            {
                label: "Riga",
                value: "LV"
            },
            {
                label: "Vilnus",
                value: "LT"
            },
            {
                label: "KIEV",
                value: "UA"
            },
            {
                label: "Moscow",
                value: "RU"
            },
            {
                label: "Warsaw",
                value: "PL"
            }
        ]
        return (
            <form 
                className="login100-form validate-form" 
                onSubmit={this.validateForm.bind(this)}
            >
                <span className="login100-form-title p-b-10">Become a Bolt driver</span>
                <span className="login100-form-subtitle p-b-30 txt">
                    If you have multiple cars or drivers
                    &nbsp;
                    <a href="#" className="txt-link">sign up as fleet owner</a>
                </span>
                <span className="label-input100">Email</span>
                <div className={`wrap-input100 validate-input m-t-5 ${this.state.errors.email ? 'has-error' : ''}`} data-validate = "Valid email is required: ex@abc.xyz">
                    <input 
                        className="input100" 
                        type="text" 
                        name="email" 
                        placeholder="Email addess..."
                        value={this.state.fields['email']} 
                        onChange={this.handleChange.bind(this, "email")}
                    />
                    <span className="focus-input100">{this.state.errors.email}</span>
                </div>
                <span className="label-input100">Phone number</span>
                <div className={`wrap-input100 validate-input m-t-5 
                                ${this.state.errors.phoneCode | this.state.errors.phoneNumber ? 'has-error' : ''}`} data-validate="Username is required">
                    <div className="row">
                        <div className="col-4">
                            <Select 
                                options={mappedCodes}
                                className="country_code" 
                                name="phoneCode"
                                onChange={this.handleChange.bind(this, "phoneCode")}
                                values={[]}
                                style={{
                                    border:"0px",
                                    outline:"0px"
                            }} />
                        </div>
                        <div className="col-8">
                            <input 
                                className="input100" 
                                type="number" 
                                name="phoneNumber"
                                value={this.state.fields['phoneNumber']} 
                                onChange={this.handleChange.bind(this, "phoneNumber")}
                            />
                        </div>
                    </div>
                    <span className="focus-input100">{this.state.errors.phoneNumber}</span>
                </div>
                <span className="label-input100">City</span>
                <div className={`wrap-input100 validate-input m-t-5 ${this.state.errors.city ? 'has-error' : ''}`} data-validate = "City is required">
                    <Select 
                        options={cities}
                        className="city" 
                        name="city"
                        onChange={this.handleChange.bind(this, "city")}
                        values={[]}
                        style={{
                            border:"0px",
                            outline:"0px"
                    }} />
                    <span className="focus-input100">{this.state.errors.city}</span>
                </div>
                <div className="flex-m w-full p-b-23 p-t-10">
                    <div className="contact100-form-checkbox">
                        <input 
                            id="ckb1" 
                            type="checkbox" 
                            name="agree"
                            value={this.state.fields['agree']}
                            onChange={this.handleChange.bind(this, "agree")}
                        />
                        <span className={`txt1 p-l-5 ${this.state.errors.agree ? 'has-error' : ''}`}>
                            I agree to Bolt's 
                            &nbsp;
                            <a href="#" className="txt-link">Terms of User</a>
                            &nbsp;
                            and
                            &nbsp;
                            <a href="#" className="txt-link">Privacy policy</a>
                        </span>
                    </div>
                </div>
    
                <div className="container-login100-form-btn">
                    <div className="wrap-login100-form-btn">
                        <div className="login100-form-bgbtn"></div>
                        <button className="login100-form-btn">SIGN UP AS DRIVER</button>
                    </div>
                </div>
            </form>
        );
    }

    validateForm(e:any) {
        e.preventDefault();
        if (this.handleValidation()) {
            alert("Form submitted");
        } else {
            alert("Form has errors.");
        }
    }

    handleValidation() {
        let fields = this.state.fields;
        let errors = this.state.errors;
        let formIsValid = true;
        
        //Phone code
        if (!fields["phoneCode"]) {
            formIsValid = false;
            errors["phoneCode"] = "Phone is required";
        }

        //Phone number
        if (!fields["phoneNumber"]) {
            formIsValid = false;
            errors["phoneNumber"] = "Phone is required";
        }

        //City
        if (!fields["city"]) {
            formIsValid = false;
            errors.city = "City is required";
        }

        //Terms and policies
        if (!fields["agree"]) {
            formIsValid = false;
            errors.agree = true;
        }
    
        //Email
        if (!fields["email"]) {
          formIsValid = false;
          errors["email"] = "Email is required";
        }
    
        if (typeof fields["email"] !== "undefined") {
          let lastAtPos = fields["email"].lastIndexOf("@");
          let lastDotPos = fields["email"].lastIndexOf(".");
    
          if (
            !(
              lastAtPos < lastDotPos &&
              lastAtPos > 0 &&
              fields["email"].indexOf("@@") == -1 &&
              lastDotPos > 2 &&
              fields["email"].length - lastDotPos > 2
            )
          ) {
            formIsValid = false;
            errors["email"] = "Email is not valid";
          }
        }
    
        this.setState({ errors: errors });
        return formIsValid;
      }
}
 
export default RegForm;


interface City {
    label: JSX.Element,
    value: string
}

interface CityObj {
    label: string,
    value: string
}

interface Errors {
    email?: string,
    phoneCode?: string,
    phoneNumber?: string,
    city?: string,
    agree?: string
}