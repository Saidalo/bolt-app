import { Component } from "react";
import headerIcon from "./img/header-icon.svg";
 
class Header extends Component {
    render() {
        return (
            <header className="site-header">
                <div className="wrapper site-header__wrapper">
                    <div className="site-header__start">
                        <a href="#" className="brand">
                            <img src={headerIcon} />
                        </a>
                    </div>
                        <div className="site-header__middle">
                            <nav className="nav">
                            <button className="nav__toggle" aria-expanded="false" type="button">
                                <img src="https://img.icons8.com/material-outlined/24/000000/menu--v1.png"/>
                            </button>
                            <ul className="nav__wrapper">
                                <li className="nav__item"><a href="#">Ride</a></li>
                                <li className="nav__item"><a href="#">Become a driver</a></li>
                                <li className="nav__item"><a href="#">Fleet</a></li>
                                <li className="nav__item"><a href="#">Bussiness</a></li>
                                <li className="nav__item"><a href="#">Scooters</a></li>
                                <li className="nav__item"><a href="#">Drive</a></li>
                                <li className="nav__item"><a href="#">Food</a></li>
                                <li className="nav__item"><a href="#">Cities</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div className="site-header__end">
                        <button type="button" className="btn btn-light login-btn">Log in</button>
                    </div>
                </div>
            </header>
        );
  }
}
 
export default Header;