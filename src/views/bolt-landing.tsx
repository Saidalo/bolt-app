import { Component } from "react";
import Header from "../components/Header/header";
import BoltPage from "../components/BoltPage/BoltPage";
import "../components/Header/css/header.css";
import "../assets/style.css";
import "../assets/reset.min.css";
 
class BoltLanding extends Component {
  render() {
    return (
        <div>
          <Header />
          <BoltPage />
        </div>
    );
  }
}
 
export default BoltLanding;